import _pyo3check

funcfunc_cast = _pyo3check.funcfunc_cast
funcfunc_extract = _pyo3check.funcfunc_extract
funcfunc_extract_rust_string = _pyo3check.funcfunc_extract_rust_string

def funcfunc_extract_need_convert(p):
    ret = _pyo3check.funcfunc_extract_need_convert(p)
    if type(p) == bytes:
        return ret.encode("utf-8")
    return ret
