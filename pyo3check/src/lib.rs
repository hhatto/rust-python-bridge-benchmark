#![feature(proc_macro, specialization)]

extern crate pyo3;

use pyo3::prelude::*;


fn pyobj2str_cast_as(obj: &PyObject) -> Result<(String, bool), String> {
    let gil = Python::acquire_gil();
    let py = gil.python();
    match obj.cast_as::<PyString>(py) {
        Ok(arg) => {
            Ok((String::from(arg.to_string_lossy()), false))
        },
        Err(_) => {
            match obj.cast_as::<PyBytes>(py) {
                Ok(arg) => {
                    Ok((String::from_utf8(arg.data().to_vec()).unwrap(), true))
                },
                Err(_) => {
                    Err("invalid argument type".to_string())
                }
            }
        },
    }
}

fn pyobj2str_extract(obj: &PyObject) -> Result<(String, bool), String> {
    let gil = Python::acquire_gil();
    let py = gil.python();
    let pystring = obj.extract::<&PyString>(py);
    match pystring {
        Ok(arg) => {
            Ok((String::from(arg.to_string_lossy()), false))
        }
        Err(_) => {
            let pybytes = obj.extract::<&PyBytes>(py);
            match pybytes {
                Ok(arg) => {
                    Ok((String::from_utf8(arg.data().to_vec()).unwrap(), true))
                },
                Err(_) => {
                    Err("invalid argument type".to_string())
                }
            }
        }
    }
}

fn pyobj2str_extract_rust_string(obj: &PyObject) -> Result<(String, bool), String> {
    let gil = Python::acquire_gil();
    let py = gil.python();
    match obj.extract::<String>(py) {
        Ok(s) => Ok((s, false)),
        Err(_) => {
            let pybytes = obj.extract::<&PyBytes>(py);
            match pybytes {
                Ok(arg) => {
                    Ok((String::from_utf8(arg.data().to_vec()).unwrap(), true))
                },
                Err(_) => {
                    Err("invalid argument type".to_string())
                }
            }
        }
    }
}

#[py::modinit(_pyo3check)]
fn init_mod(py: Python, m: &PyModule) -> PyResult<()> {

    #[pyfn(m, "funcfunc_extract_rust_string")]
    pub fn _extract_rust_string(path_str: PyObject) -> PyResult<PyObject> {
        let arg_str = pyobj2str_extract_rust_string(&path_str);
        match arg_str {
            Err(e) => return Err(exc::TypeError::new(e)),
            _ => {}
        }
        let (arg_str, is_bytes) = arg_str.unwrap();
        let gil = Python::acquire_gil();
        let py = gil.python();
        if is_bytes {
            Ok(PyBytes::new(py, arg_str.as_bytes()).to_object(py))
        } else {
            Ok(PyString::new(py, arg_str.as_str()).to_object(py))
        }
    }

    #[pyfn(m, "funcfunc_extract_need_convert")]
    pub fn _extract_need_convert(path_str: PyObject) -> PyResult<String> {
        let arg_str = pyobj2str_extract(&path_str);
        match arg_str {
            Err(e) => return Err(exc::TypeError::new(e)),
            _ => {}
        }
        let (arg_str, _is_bytes) = arg_str.unwrap();
        Ok(arg_str)
    }

    #[pyfn(m, "funcfunc_extract")]
    pub fn _extract(path_str: PyObject) -> PyResult<PyObject> {
        let arg_str = pyobj2str_extract(&path_str);
        match arg_str {
            Err(e) => return Err(exc::TypeError::new(e)),
            _ => {}
        }
        let (arg_str, is_bytes) = arg_str.unwrap();
        let gil = Python::acquire_gil();
        let py = gil.python();
        if is_bytes {
            Ok(PyBytes::new(py, arg_str.as_bytes()).to_object(py))
        } else {
            Ok(PyString::new(py, arg_str.as_str()).to_object(py))
        }
    }

    #[pyfn(m, "funcfunc_cast")]
    pub fn _cast(path_str: PyObject) -> PyResult<PyObject> {
        let arg_str = pyobj2str_cast_as(&path_str);
        match arg_str {
            Err(e) => return Err(exc::TypeError::new(e)),
            _ => {}
        }
        let (arg_str, is_bytes) = arg_str.unwrap();
        let gil = Python::acquire_gil();
        let py = gil.python();
        if is_bytes {
            Ok(PyBytes::new(py, arg_str.as_bytes()).to_object(py))
        } else {
            Ok(PyString::new(py, arg_str.as_str()).to_object(py))
        }
    }

    Ok(())
}
