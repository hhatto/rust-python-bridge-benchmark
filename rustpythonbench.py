import pyo3check
import rcpycheck
from benchmarker import Benchmarker

n = 1000 * 2000
sdata10 = "0123456789"
bdata10 = b"0123456789"
sdata100 = "0123456789" * 10
bdata100 = b"0123456789" * 10
sdata1000 = "0123456789" * 100
bdata1000 = b"0123456789" * 100

def native(sorb):
    _ = sorb
    return sorb

# pyo3
with Benchmarker(n, width=50) as b:
    @b("native.len10")
    def _(bm):
        func = native
        assert((func(sdata10)) != str)
        assert((func(bdata10)) != bytes)
        for i in bm:
            func(sdata10)
            func(bdata10)

    @b("(pyo3) cast.len10")
    def _(bm):
        func = pyo3check.funcfunc_cast
        assert((func(sdata10)) != str)
        assert((func(bdata10)) != bytes)
        for i in bm:
            func(sdata10)
            func(bdata10)

    @b("(pyo3) extract.len10")
    def _(bm):
        func = pyo3check.funcfunc_extract
        assert((func(sdata10)) != str)
        assert((func(bdata10)) != bytes)
        for i in bm:
            func(sdata10)
            func(bdata10)

    @b("(pyo3) extract_rust_string.len10")
    def _(bm):
        func = pyo3check.funcfunc_extract_rust_string
        assert((func(sdata10)) != str)
        assert((func(bdata10)) != bytes)
        for i in bm:
            func(sdata10)
            func(bdata10)

    @b("(pyo3) extract_need_conv.len10")
    def _(bm):
        func = pyo3check.funcfunc_extract_need_convert
        assert((func(sdata10)) != str)
        assert((func(bdata10)) != bytes)
        for i in bm:
            func(sdata10)
            func(bdata10)

    @b("(pyo3) cast.len100")
    def _(bm):
        func = pyo3check.funcfunc_cast
        for i in bm:
            func(sdata100)
            func(bdata100)

    @b("(pyo3) extract.len100")
    def _(bm):
        func = pyo3check.funcfunc_extract
        for i in bm:
            func(sdata100)
            func(bdata100)

    @b("(pyo3) extract_rust_string.len100")
    def _(bm):
        func = pyo3check.funcfunc_extract_rust_string
        for i in bm:
            func(sdata100)
            func(bdata100)

    @b("(pyo3) cast.len1000")
    def _(bm):
        func = pyo3check.funcfunc_cast
        for i in bm:
            func(sdata1000)
            func(bdata1000)

    @b("(pyo3) extract.len1000")
    def _(bm):
        func = pyo3check.funcfunc_extract
        for i in bm:
            func(sdata1000)
            func(bdata1000)

    @b("(pyo3) extract_rust_string.len1000")
    def _(bm):
        func = pyo3check.funcfunc_extract_rust_string
        for i in bm:
            func(sdata1000)
            func(bdata1000)

    @b("(rust-cpython) cast.len10")
    def _(bm):
        func = rcpycheck.funcfunc_cast
        assert((func(sdata10)) != str)
        assert((func(bdata10)) != bytes)
        for i in bm:
            func(sdata10)
            func(bdata10)

    @b("(rust-cpython) extract.len10")
    def _(bm):
        func = rcpycheck.funcfunc_extract
        assert((func(sdata10)) != str)
        assert((func(bdata10)) != bytes)
        for i in bm:
            func(sdata10)
            func(bdata10)

    @b("(rust-cpython) extract_rust_string.len10")
    def _(bm):
        func = rcpycheck.funcfunc_extract_rust_string
        assert((func(sdata10)) != str)
        assert((func(bdata10)) != bytes)
        for i in bm:
            func(sdata10)
            func(bdata10)

    @b("(rust-cpython) extract_need_conv.len10")
    def _(bm):
        func = rcpycheck.funcfunc_extract_need_convert
        assert((func(sdata10)) != str)
        assert((func(bdata10)) != bytes)
        for i in bm:
            func(sdata10)
            func(bdata10)

    @b("(rust-cpython) cast.len100")
    def _(bm):
        func = rcpycheck.funcfunc_cast
        for i in bm:
            func(sdata100)
            func(bdata100)

    @b("(rust-cpython) extract.len100")
    def _(bm):
        func = rcpycheck.funcfunc_extract
        for i in bm:
            func(sdata100)
            func(bdata100)

    @b("(rust-cpython) extract_rust_string.len100")
    def _(bm):
        func = rcpycheck.funcfunc_extract_rust_string
        for i in bm:
            func(sdata100)
            func(bdata100)

    @b("(rust-cpython) cast.len1000")
    def _(bm):
        func = rcpycheck.funcfunc_cast
        for i in bm:
            func(sdata1000)
            func(bdata1000)

    @b("(rust-cpython) extract.len1000")
    def _(bm):
        func = rcpycheck.funcfunc_extract
        for i in bm:
            func(sdata1000)
            func(bdata1000)

    @b("(rust-cpython) extract_rust_string.len1000")
    def _(bm):
        func = rcpycheck.funcfunc_extract_rust_string
        for i in bm:
            func(sdata1000)
            func(bdata1000)
