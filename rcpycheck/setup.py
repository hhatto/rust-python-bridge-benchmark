from setuptools import setup, find_packages
from setuptools_rust import Binding, RustExtension


setup(name='rcpycheck',
      version='0.1',
      packages=find_packages(),
      rust_extensions=[
          RustExtension('_rcpycheck', 'Cargo.toml', binding=Binding.RustCPython)],
      # rust extensions are not zip safe, just like C-extensions.
      zip_safe=False)
