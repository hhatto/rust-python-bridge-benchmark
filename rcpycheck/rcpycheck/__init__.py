import _rcpycheck

funcfunc_cast = _rcpycheck.funcfunc_cast
funcfunc_extract = _rcpycheck.funcfunc_extract
funcfunc_extract_rust_string = _rcpycheck.funcfunc_extract_rust_string

def funcfunc_extract_need_convert(p):
    ret = _rcpycheck.funcfunc_extract_need_convert(p)
    if type(p) == bytes:
        return ret.encode("utf-8")
    return ret
