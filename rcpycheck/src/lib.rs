#[macro_use] extern crate cpython;

use cpython::{PyResult, Python, PyObject, PyString, PyBytes, PyErr, PythonObject};
use cpython::exc;

fn pyobj2str_cast_as(py: &Python, obj: &PyObject) -> Result<(String, bool), String> {
    //let gil = Python::acquire_gil();
    //let py = gil.python();
    match obj.cast_as::<PyString>(*py) {
        Ok(arg) => {
            Ok((String::from(arg.to_string_lossy(*py)), false))
        },
        Err(_) => {
            match obj.cast_as::<PyBytes>(*py) {
                Ok(arg) => {
                    Ok((String::from_utf8(arg.data(*py).to_vec()).unwrap(), true))
                },
                Err(_) => {
                    Err("invalid argument type".to_string())
                }
            }
        },
    }
}

fn pyobj2str_extract(py: &Python, obj: &PyObject) -> Result<(String, bool), String> {
    let pystring = obj.extract::<&PyString>(*py);
    match pystring {
        Ok(arg) => {
            Ok((String::from(arg.to_string_lossy(*py)), false))
        }
        Err(_) => {
            let pybytes = obj.extract::<&PyBytes>(*py);
            match pybytes {
                Ok(arg) => {
                    Ok((String::from_utf8(arg.data(*py).to_vec()).unwrap(), true))
                },
                Err(_) => {
                    Err("invalid argument type".to_string())
                }
            }
        }
    }
}

fn pyobj2str_extract_rust_string(py: &Python, obj: &PyObject) -> Result<(String, bool), String> {
    match obj.extract::<String>(*py) {
        Ok(s) => Ok((s, false)),
        Err(_) => {
            let pybytes = obj.extract::<&PyBytes>(*py);
            match pybytes {
                Ok(arg) => {
                    Ok((String::from_utf8(arg.data(*py).to_vec()).unwrap(), true))
                },
                Err(_) => {
                    Err("invalid argument type".to_string())
                }
            }
        }
    }
}

py_module_initializer!(_rcpycheck, init_rcpycheck, PyInit__rcpycheck, |py, m| {
    try!(m.add(py, "__doc__", "rust-cpython strcheck moudle"));
    try!(m.add(py, "funcfunc_extract_rust_string", py_fn!(py, _extract_rust_string(path_str: PyObject))));
    try!(m.add(py, "funcfunc_extract_need_convert", py_fn!(py, _extract_need_convert(path_str: PyObject))));
    try!(m.add(py, "funcfunc_extract", py_fn!(py, _extract(path_str: PyObject))));
    try!(m.add(py, "funcfunc_cast", py_fn!(py, _cast(path_str: PyObject))));
    Ok(())
});

fn _extract_rust_string(py: Python, path_str: PyObject) -> PyResult<PyObject> {
    let arg_str = pyobj2str_extract_rust_string(&py, &path_str);
    match arg_str {
        Err(_) => return Err(PyErr::new_lazy_init(py.get_type::<exc::TypeError>(), None)),
        _ => {}
    }
    let (arg_str, is_bytes) = arg_str.unwrap();
    if is_bytes {
        Ok(PyBytes::new(py, arg_str.as_bytes()).into_object())
    } else {
        Ok(PyString::new(py, arg_str.as_str()).into_object())
    }
}

fn _extract_need_convert(py: Python, path_str: PyObject) -> PyResult<String> {
    let arg_str = pyobj2str_extract(&py, &path_str);
    match arg_str {
        Err(_) => return Err(PyErr::new_lazy_init(py.get_type::<exc::TypeError>(), None)),
        _ => {}
    }
    let (arg_str, _is_bytes) = arg_str.unwrap();
    Ok(arg_str)
}

fn _extract(py: Python, path_str: PyObject) -> PyResult<PyObject> {
    let arg_str = pyobj2str_extract(&py, &path_str);
    match arg_str {
        Err(_) => return Err(PyErr::new_lazy_init(py.get_type::<exc::TypeError>(), None)),
        _ => {}
    }
    let (arg_str, is_bytes) = arg_str.unwrap();
    if is_bytes {
        Ok(PyBytes::new(py, arg_str.as_bytes()).into_object())
    } else {
        Ok(PyString::new(py, arg_str.as_str()).into_object())
    }
}

fn _cast(py: Python, path_str: PyObject) -> PyResult<PyObject> {
    let arg_str = pyobj2str_cast_as(&py, &path_str);
    match arg_str {
        Err(_) => return Err(PyErr::new_lazy_init(py.get_type::<exc::TypeError>(), None)),
        _ => {}
    }
    let (arg_str, is_bytes) = arg_str.unwrap();
    if is_bytes {
        Ok(PyBytes::new(py, arg_str.as_bytes()).into_object())
    } else {
        Ok(PyString::new(py, arg_str.as_str()).into_object())
    }
}
